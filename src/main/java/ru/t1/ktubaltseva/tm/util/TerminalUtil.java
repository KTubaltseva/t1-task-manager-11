package ru.t1.ktubaltseva.tm.util;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextInt() {
        final String string = nextLine();
        return Integer.parseInt(string);
    }

}
