package ru.t1.ktubaltseva.tm.api.repository;

import ru.t1.ktubaltseva.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project create(String name, String description);
    Project create(String name);

    Project add(Project project);

    void clear();

    List<Project> findAll();

    Project findOneByIndex(Integer index);

    Project findOneById(String id);

    Project remove(Project project);

    Project removeByIndex(Integer index);

    Project removeById(String id);

}
