package ru.t1.ktubaltseva.tm.api.repository;

import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

public interface ITaskRepository {


    Task add(Task task);

    void clear();

    List<Task> findAll();

    Task create(String name, String description);

    Task create(String name);

    Task findOneByIndex(Integer index);

    Task findOneById(String id);

    Task remove(Task task);

    Task removeByIndex(Integer index);

    Task removeById(String id);

}
