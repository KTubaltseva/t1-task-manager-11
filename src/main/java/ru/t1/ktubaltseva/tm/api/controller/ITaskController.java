package ru.t1.ktubaltseva.tm.api.controller;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void displayTasks();

    void displayTasksFullInfo();

    void removeTaskById();

    void removeTaskByIndex();

    void displayTaskById();

    void displayTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

}
