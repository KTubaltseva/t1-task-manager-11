package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task add(Task task);

    void clear();

    List<Task> findAll();

    Task create(String name, String description);

    Task create(String name);

    void remove(Task task);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task removeById(String id);

    Task removeByIndex(Integer Index);

}
