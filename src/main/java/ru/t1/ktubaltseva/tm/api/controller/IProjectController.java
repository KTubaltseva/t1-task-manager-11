package ru.t1.ktubaltseva.tm.api.controller;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void displayProjects();

    void displayProjectsFullInfo();

    void removeProjectById();

    void removeProjectByIndex();

    void displayProjectById();

    void displayProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}
