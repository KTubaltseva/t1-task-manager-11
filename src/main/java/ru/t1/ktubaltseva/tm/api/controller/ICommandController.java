package ru.t1.ktubaltseva.tm.api.controller;

public interface ICommandController {

    void displayVersion();

    void displayAbout();

    void displayHelp();

    void displayArgumentError();

    void displayCommandError();

    void displaySystemInfo();

    void exit();

}
