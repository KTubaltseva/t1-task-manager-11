package ru.t1.ktubaltseva.tm.controller;

import ru.t1.ktubaltseva.tm.api.controller.ITaskController;
import ru.t1.ktubaltseva.tm.api.service.ITaskService;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask(){
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine() ;
        Task task = taskService.create(name, description);
        if (task == null) System.err.println("ERROR");
        else {
            System.out.println(task);
            System.out.println("[OK]");
        }
    }

    @Override
    public void clearTasks(){
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void displayTasks(){
        System.out.println("[DISPLAY TASKS]");
        int index = 1;
        List<Task> tasks = taskService.findAll();
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void displayTasksFullInfo(){
        System.out.println("[DISPLAY TASKS FULL]");
        int index = 1;
        List<Task> tasks = taskService.findAll();
        for (final Task task: tasks) {
            System.out.println(index + ".");
            displayTaskFull(task);
            System.out.println();
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) System.err.println("FAIL");
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.err.println("FAIL");
        System.out.println("[OK]");
    }

    @Override
    public void displayTaskById() {
        System.out.println("[DISPLAY PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.err.println("FAIL");
            return;
        }
        displayTaskFull(task);
        System.out.println("[OK]");
    }

    @Override
    public void displayTaskByIndex() {
        System.out.println("[DISPLAY PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.err.println("FAIL");
            return;
        }
        displayTaskFull(task);
        System.out.println("[OK]");
    }

    public void displayTaskFull(final Task task){
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESC: " + task.getDescription());
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateById(id, name, description);
        if (task == null) {
            System.err.println("FAIL");
            return;
        }
        displayTaskFull(task);
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextInt() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) {
            System.err.println("FAIL");
            return;
        }
        displayTaskFull(task);
        System.out.println("[OK]");
    }

}
